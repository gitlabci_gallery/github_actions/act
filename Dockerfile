FROM registry.gitlab.inria.fr/inria-ci/docker/ubuntu
# see https://stackoverflow.com/questions/44331836/apt-get-install-tzdata-noninteractive
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install --yes curl sudo
RUN curl -s https://raw.githubusercontent.com/nektos/act/master/install.sh | sudo bash
