# Running [GitHub Actions](https://github.com/features/actions) in GitLab with [`act`](https://github.com/nektos/act)

## Why use [GitHub Actions](https://github.com/features/actions) within GitLab?

[GitHub Actions](https://github.com/features/actions) is a platform
and a language to specify continuous integration pipelines for
GitHub-hosted projects. Actions can be easily composed and re-used
between projects.

Using the same language to describe pipelines for projects hosted in
gitlab.inria.fr can be convenient if some actions can be re-used from
other GitHub-hosted projects. For instance, if the project is the
non-public part of a bigger project hosted on GitHub, or if the
project is hosted on GitHub but is mirrored on GitLab so as to have
a private sandbox to make experiments before publishing them, then
this makes sense for the pipelines to be described as GitHub Actions,
while being executed in the continuous integration platform provided
by gitlab.inria.fr.

## Prerequisites

This project requires the following common prerequisites.
See the [intro](https://gitlab.inria.fr/gitlabci_gallery/intro) project for
details.

- Enable project CI/CD feature
- Enable shared runners
- Enable container registry

## The [`act`](https://github.com/nektos/act) command-line tool

The GitLab pipeline uses the [`act`](https://github.com/nektos/act)
command-line tool to run the
[GitHub Actions](https://github.com/features/actions)
pipelines.

[`act`](https://github.com/nektos/act) is a tool that reproduces
partially the environment provided by GitHub. There are some
limitations on the kind of actions it can run, and some workarounds
that are documented in the [`act`](https://github.com/nektos/act)
project page.

## The pipeline specification [`.gitlab-ci.yml`](.gitlab-ci.yml)

The pipeline uses
[`register-dockerfile.yml`](https://gitlab.inria.fr/inria-ci/docker/-/tree/main/register-dockerfile)
template to prepare a Docker image
with the environment specified in [`Dockerfile`](Dockerfile).

The [`act`](https://github.com/nektos/act) command-line tool is
installed in this container, so that this installation is cached in
the container registry, not to have to reinstall the tool at every commit
but only when the environment is changed.

In addition to the jobs `prepare` and `clean`,
associated to the template above,
the pipeline defines the following job which runs `act` in the container
prepared by the template, which executes the
workflows specified in GitHub Actions.

```yaml
build:
  stage: build
  tags:
    - linux
    - small
  image: "$image"
  script:
    - act -P ubuntu-latest=ghcr.io/catthehacker/ubuntu:act-latest
```

## The GitHub Actions workflow [`.github/workflows/ci.yml`](.github/workflows/ci.yml)

GitHub Actions workflows are stored in `.github/workflows/` sub-directory.
Our example checkouts the repository, run `make`, execute the compiled
`hello_world` program, and compares its output with `expected.txt`.
